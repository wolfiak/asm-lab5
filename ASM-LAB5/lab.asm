.386
.MODEL flat,STDCALL

STD_INPUT_HANDLE equ -10
STD_OUTPUT_HANDLE equ -11

GetStdHandle PROTO :DWORD
ExitProcess PROTO : DWORD
wsprintfA PROTO C :VARARG
WriteConsoleA PROTO : DWORD, :DWORD, :DWORD, :DWORD, :DWORD
ReadConsoleA  PROTO :DWORD, :DWORD, :DWORD, :DWORD, :DWORD
atoi PROTO :DWORD
.DATA
	
	write DWORD ?
	read DWORD ?

	tablica DWORD 4 DUP(?)

	tekst BYTE "Wprowadz liczbe: ",10,0
	rozmiart DWORD $ - tekst

	tekst2 BYTE "Nie rowne ",10,0
	rozmiart2 DWORD $ - tekst2

	tekst3 BYTE " rowne ",10,0
	rozmiart3 DWORD $ - tekst3

	zmienna1 BYTE ?
	zmienna2 BYTE ?

	lZnakow DWORD 0
	lZnakowW DWORD 0

	bufor BYTE 10 DUP(?)
.CODE
main proc
		invoke GetStdHandle, STD_INPUT_HANDLE
		mov read, EAX

		invoke GetStdHandle, STD_OUTPUT_HANDLE
		mov write, EAX

		mov EDI, OFFSET tablica
		mov ECX,2
		Wprowadzanie:
		push ECX
		push EDI

		invoke WriteConsoleA, write, OFFSET tekst, rozmiart, OFFSET lZnakow, 0
		invoke ReadConsoleA, read, OFFSET bufor, 10, OFFSET lZnakowW, 0
		mov EBX, OFFSET bufor
		add EBX, lZnakowW
		mov [EBX -2], BYTE PTR 0
		invoke atoi, OFFSET bufor
		pop EDI 
		stosd

		pop ECX
		loop Wprowadzanie

		mov EBX, OFFSET tablica
		mov EAX, [EBX]
		mov zmienna1, al

		mov EBX, OFFSET tablica
		mov EAX, [EBX+4]
		mov zmienna2, al

		mov AL, zmienna1
		mov BL, zmienna2

		cmp AL,BL
		je Rowne
		invoke WriteConsoleA, write, OFFSET tekst2, rozmiart2, OFFSET lZnakow, 0
		Powrot:
		invoke ExitProcess, 0

		Rowne:
			invoke WriteConsoleA, write, OFFSET tekst3, rozmiart3, OFFSET lZnakow, 0
		jmp Powrot


main endp




atoi proc uses esi edx inputBuffAddr:DWORD
	mov esi, inputBuffAddr
	xor edx, edx
	xor EAX, EAX
	mov AL, BYTE PTR [esi]
	cmp eax, 2dh
	je parseNegative

	.Repeat
		lodsb
		.Break .if !eax
		imul edx, edx, 10
		sub eax, "0"
		add edx, eax
	.Until 0
	mov EAX, EDX
	jmp endatoi

	parseNegative:
	inc esi
	.Repeat
		lodsb
		.Break .if !eax
		imul edx, edx, 10
		sub eax, "0"
		add edx, eax
	.Until 0

	xor EAX,EAX
	sub EAX, EDX
	jmp endatoi

	endatoi:
	ret
atoi endp


END	